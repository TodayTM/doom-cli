# Doom CLI

## About:
Doom-cli is an open-source version of doom, it made for unix like operating systems (linux, mac OS, BSD etc...)

## Instal:
Open a terminal and paste this:
``` sh
cd ~
git clone https://gitlab.com/TodayTM/doom-cli 
cd ~/doom-cli
sudo sh ~/doom-cli/install.sh
```

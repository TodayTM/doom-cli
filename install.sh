#!/bin/sh

sudo rm -rf /opt/doom-cli
sudo mkdir -p /opt/doom-cli/
sudo cp ~/doom-cli/keyboard.vlk /opt/doom-cli/keyboard.vlk
sudo cp doom-cli.sh /usr/bin/doom-cli
